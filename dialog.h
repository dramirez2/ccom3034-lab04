//DANIEL RAMIREZ && RAMON COLLAZO
//CCOM3034-LAB04 && RAFAEL ARCE

#ifndef DIALOG_H
#define DIALOG_H
#include <QtGui>
#include <QtCore>
#include <QDialog>
#include <QTimer>

class Dialog : public QDialog
{
    Q_OBJECT
protected:
    void mousePressEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent * event);
    void Diagmove(QKeyEvent * event);
    int x; //Holds the x corrdinate
    int y; //Holds the y coodinate
    bool turnRight; //used to determine if it turns right
    bool turnRigth; //used to determine if it turns right
    bool turnLeft; //Used to determine if it turns left
    bool turnUp;    //Used to determine if it goes up
    bool turnDown; //Used to determine if it goes down
    QTimer *myTimer;
public:
    Dialog(QWidget *parent = 0);
    ~Dialog();

public slots:
    void mySlot();
};

#endif // DIALOG_H
