//DANIEL RAMIREZ && RAMON COLLAZO
//CCOM3034-LAB04 && RAFAEL ARCE

#include <QApplication>
#include "dialog.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Dialog w;
    w.show();

    return a.exec();
}
