//DANIEL RAMIREZ && RAMON COLLAZO
//CCOM3034-LAB04 && RAFAEL ARCE

#include "dialog.h"
#include<QtDebug>
Dialog::Dialog(QWidget *parent)
    : QDialog(parent){
    x=width()/2;        //By default, it puts the ball in the center using
    y= height()/2;      //these x and y coordinates
    turnRight =true;    //Used to determine if it turns right
    turnLeft = false;   //Used to determine if it turns left
    turnUp = true;      //Used to determine if it goes up
    turnDown = false;   //Used to determine if it goes down
    myTimer = new QTimer(this);

    // this makes the mySlot function be called each time the time ticks
    connect(myTimer, SIGNAL(timeout()), this, SLOT(mySlot()));

    // start the timer to tick every 25 ms
    myTimer->start(25);
}

Dialog::~Dialog()
{

}

void Dialog::mousePressEvent(QMouseEvent *event){
    qDebug() << "clicked at: " << event->x() << "," << event->y();
    if (event->button() == Qt::LeftButton){
        qDebug() << "the button clicked was left";
    }

    else if (event->button() == Qt::RightButton) {
        qDebug() << "The button clicked was right";
    }
    x=rand() % width();
    y=rand() % height();
    repaint();

}


void Dialog::paintEvent(QPaintEvent *event) {
    // the QPainter is the 'canvas' to which we will draw
    // the QPen is the pen that will be used to draw to the 'canvas'
    QPainter p(this);
    QPen myPen;

    myPen.setWidth(10);
    myPen.setColor(QColor(0xff0000));
    p.setPen(myPen);

    p.drawEllipse(x,y,20,20);

}

void Dialog::keyPressEvent(QKeyEvent * event){
    //Moves ball left
    if (event->key() == Qt::Key_Left){
        x-=10;
        repaint();
      }
    //Moves ball right
    else if (event->key() == Qt::Key_Right){
        x+=10;
        repaint();
    }
    //Moves ball up
    else if (event->key() == Qt::Key_Up){
        y-=10;
        repaint();
    }
    //Moves ball down
    else if (event->key() == Qt::Key_Down){
        y+=10;
        repaint();
    }
    //Moves ball diagonally up to left
    else if (event->key() == Qt::Key_Q){
        x-=10;
        y-=10;
        repaint();
    }

    //Moves ball diagonally down to left
    else if (event->key() == Qt::Key_W){
        x-=10;
        y+=10;
        repaint();
    }
    //Moves ball diagonally down to right
    else if (event->key() == Qt::Key_E){
        x+=10;
        y-=10;
        repaint();
    }
    //Moves ball diagonally up to right
    else if (event->key() == Qt::Key_R){
        x+=10;
        y+=10;
        repaint();
    }
}
void Dialog::mySlot() {
   // qDebug() << "tick";

    //If the ball is going to the right..
    if(turnRight) {
        //If the actual x is valid to the screen width and
        //is going up then change the values (x,y)
        if(x < width() && turnUp){
            x+=10;
            y-=10;
            //If, by changing y, it gets out of the screen
            //(in the top side) then switch up and down values
            if (y < 0){
                turnUp = false;
                turnDown = true;
            }
         }
        //If the actual x is valid to the screen width and
        //is going down then change the values (x,y)
        else if ( x < width() && turnDown){
            x+=10;
            y+=10;
            //If, by changing y, it gets out of the screen
            //(by the bottom side) then switch up and down values
            if (y > height()-20){
                turnDown = false;
                turnUp = true;
            }
        }
        //If the actual x exceeds the width of the screen
        //then switch right and left
        if (x >= width()-20){
                turnRight = false;
                turnLeft = true;
        }

    }
    //Else, if the ball is going to the left..
    else if (turnLeft){
        //If the actual x is valid to the screen width and
        //is going down, then change the values (x,y)
        if (x > 0 && turnDown) {
            x-=10;
            y+=10;
            //If, by changing y, it gets out of the screen
            //(by the bottom side) then switch up and down values
            if (y > height()-20){
                turnDown = false;
                turnUp = true;
            }

        }
        //If the actual x is valid to the screen width and
        //is going up, then change the values (x,y)
        if (x > 0 && turnUp){
            x-=10;
            y-=10;
            //If, by changing y, it gets out of the screen
            //(by the top side) then switch up and down values
            if (y < 0){
                turnUp =false;
                turnDown = true;
            }
        }
        //If the actual x exceeds the width of the screen
        //then switch right and left
        if (x <= 0){
            turnLeft = false;
            turnRight = true;
        }

    }
    //repaint the new position of the ball
    repaint();
}

